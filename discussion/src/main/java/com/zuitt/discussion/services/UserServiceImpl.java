package com.zuitt.discussion.services;


import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepo;

    public void createUser(User user){
        userRepo.save(user);
    }

    public Iterable<User> getUsers() {
        return userRepo.findAll();
    }


    public ResponseEntity deleteUser(Long id) {
        userRepo.deleteById(id);
        return new ResponseEntity<>("Post deleted Successfully", HttpStatus.OK);
    }


    public ResponseEntity updateUser(Long id, User user) {
        User userForUpdate = userRepo.findById(id).get();
        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());
        userRepo.save(userForUpdate);
        return new ResponseEntity<>("Post updated successfully",HttpStatus.OK);
    }
}
